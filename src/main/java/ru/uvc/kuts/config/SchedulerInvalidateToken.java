package ru.uvc.kuts.config;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.uvc.kuts.config.annotation.AuthTokenMap;
import ru.uvc.kuts.vo.TokenAuth;

import java.time.LocalDateTime;
import java.util.Map;

@Component
public class SchedulerInvalidateToken {
    private final Map<String, TokenAuth> tokenAuthMap;

    public SchedulerInvalidateToken(@AuthTokenMap Map<String, TokenAuth> tokenAuthMap) {
        this.tokenAuthMap = tokenAuthMap;
    }

    @Scheduled(fixedRate = 900_000)
    public void invalidateToken() {
        tokenAuthMap.entrySet()
                .removeIf(auth -> auth.getValue().getExpiredDate().isBefore(LocalDateTime.now()));
    }

}
