package ru.uvc.kuts.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.uvc.kuts.config.annotation.AuthTokenMap;
import ru.uvc.kuts.vo.TokenAuth;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class BaseConfiguration {

    @Bean
    @AuthTokenMap
    public Map<String, TokenAuth> authTokenMap() {
        return new ConcurrentHashMap<>();
    }

}
