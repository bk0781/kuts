package ru.uvc.kuts.config.annotation;

import org.springframework.web.bind.annotation.Mapping;
import ru.uvc.kuts.vo.Role;

import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Mapping
public @interface UserAuth {
    Role value();
}
