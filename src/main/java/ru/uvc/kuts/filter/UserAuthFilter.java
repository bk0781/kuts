package ru.uvc.kuts.filter;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ru.uvc.kuts.config.annotation.AuthTokenMap;
import ru.uvc.kuts.config.annotation.UserAuth;
import ru.uvc.kuts.vo.Role;
import ru.uvc.kuts.vo.TokenAuth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Map;

@Component
public class UserAuthFilter extends HandlerInterceptorAdapter {
    private static String AUTH_HEADER = "Authorization";

    private final Map<String, TokenAuth> tokenAuthMap;


    public UserAuthFilter(@AuthTokenMap Map<String, TokenAuth> tokenAuthMap) {
        this.tokenAuthMap = tokenAuthMap;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) { return true; }

        HandlerMethod handlerMethod = (HandlerMethod) handler;

        UserAuth userAuthAnnotation = handlerMethod.getMethodAnnotation(UserAuth.class);
        if (userAuthAnnotation == null) { return true; }

        String authToken = getAuthToken(request);

        TokenAuth tokenAuth = tokenAuthMap.get(authToken);
        if (tokenAuth == null || tokenAuth.getExpiredDate().isBefore(LocalDateTime.now())) {
            throw new HttpClientErrorException(HttpStatus.FORBIDDEN);
        }

        if (!tokenAuth.getUserDTO().getRole().equals(Role.ADMIN)
                && !tokenAuth.getUserDTO().getRole().equals(userAuthAnnotation.value())) {
            throw new HttpClientErrorException(HttpStatus.FORBIDDEN);
        }

        request.setAttribute("user", tokenAuth.getUserDTO());
        return true;
    }

    private String getAuthToken(HttpServletRequest request) {
        try {
            return request.getHeaders(AUTH_HEADER).nextElement();
        } catch (Exception e) {
            throw new HttpClientErrorException(HttpStatus.FORBIDDEN);
        }
    }
}
