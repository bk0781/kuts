package ru.uvc.kuts.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.uvc.kuts.config.annotation.UserAuth;
import ru.uvc.kuts.dto.ResultTestDTO;
import ru.uvc.kuts.dto.UserDTO;
import ru.uvc.kuts.entities.Node;
import ru.uvc.kuts.entities.Test;
import ru.uvc.kuts.entities.TestResult;
import ru.uvc.kuts.entities.User;
import ru.uvc.kuts.service.NodeService;
import ru.uvc.kuts.service.TestResultService;
import ru.uvc.kuts.service.TestService;
import ru.uvc.kuts.vo.Role;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class TestController {
    private final TestService testService;
    private final TestResultService testResultService;

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/test", method = RequestMethod.POST)
    public Test saveTest(@RequestBody Test test) {
        return testService.save(test);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/test/{nodeId}", method = RequestMethod.GET)
    public Test getTestByNodeId(@PathVariable long nodeId) {
        return testService.getTestByNodeId(nodeId);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/test/{nodeId}/start", method = RequestMethod.GET)
    public Test getTestByNodeIdExcludeCorrect(@PathVariable long nodeId) {
        return testService.getTestByNodeIdExcludeCorrect(nodeId);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/test/{nodeId}/validate", method = RequestMethod.POST)
    public ResultTestDTO getTestByNodeIdExcludeCorrect(@RequestBody Test test, HttpServletRequest request) {
        return testService.validateTest(test, (UserDTO) request.getAttribute("user"));
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/test/testResult/{userId}", method = RequestMethod.GET)
    public List<TestResult> getTestResults(@PathVariable long userId) {
        return testResultService.findAllTestResultsByUserId(userId);
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/test/{testId}", method = RequestMethod.DELETE)
    public void removeTest(@PathVariable long testId) {
        testService.remove(testId);
    }

}
