package ru.uvc.kuts.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.uvc.kuts.config.annotation.UserAuth;
import ru.uvc.kuts.entities.Node;
import ru.uvc.kuts.service.FileService;
import ru.uvc.kuts.service.NodeService;
import ru.uvc.kuts.vo.Role;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class NodeController {
    private final NodeService nodeService;
    private final FileService fileService;

    @RequestMapping(path = "/api/1/node", method = RequestMethod.POST)
    public Node saveNode(@RequestBody Node node) {
        if (node.getHasContent() == null) {
            node.setHasContent(false);
        }

        if (node.getHasTest() == null) {
            node.setHasTest(false);
        }
        return nodeService.save(node);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/node/{id}", method = RequestMethod.GET)
    public Node getNodeById(@PathVariable long id) {
        return nodeService.getById(id);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/node/findAll", method = RequestMethod.GET)
    public List<Node> getAllNodes() {
        return nodeService.getRootNodes(nodeService.getAll());
    }

    @RequestMapping(path = "/api/1/node/{id}", method = RequestMethod.DELETE)
    public void removeNode(@PathVariable long id) {
        String filename = nodeService.removeNode(id);
        if (filename != null) {
            fileService.deleteFile(filename);
        }
    }

    @RequestMapping(path = "/api/1/node", method = RequestMethod.PUT)
    public Node updateNode(@RequestBody Node node) {
        return nodeService.save(node);
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/node/order/parent/{parentId}", method = RequestMethod.GET)
    public Long getNextOrder(@PathVariable Long parentId) {
        Long result = nodeService.nextOrder(parentId == 0 ? null : parentId);
        return result == null ? 0 : result;
    }
}
