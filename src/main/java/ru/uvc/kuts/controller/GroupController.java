package ru.uvc.kuts.controller;

import org.springframework.web.bind.annotation.*;
import ru.uvc.kuts.config.annotation.UserAuth;
import ru.uvc.kuts.entities.Group;
import ru.uvc.kuts.service.GroupService;
import ru.uvc.kuts.vo.Role;

import java.util.List;

@RestController
public class GroupController {
    private final GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/group", method = RequestMethod.POST)
    public Group createGroup(@RequestBody Group group) {
        return groupService.createGroup(group);
    }

    @RequestMapping(path = "/api/1/group/list", method = RequestMethod.GET)
    public List<Group> getAllGroups() {
        return groupService.getAllGroups();
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/group/{groupId}", method = RequestMethod.GET)
    public Group getGroupById(@PathVariable("groupId") long groupId) {
        return groupService.findById(groupId);
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/group/{groupId}", method = RequestMethod.DELETE)
    public void deleteGroupById(@PathVariable("groupId") long groupId) {
        groupService.deleteById(groupId);
    }
}
