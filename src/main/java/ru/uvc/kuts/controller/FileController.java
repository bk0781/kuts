package ru.uvc.kuts.controller;

import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.uvc.kuts.service.ContentService;
import ru.uvc.kuts.service.FileService;

import java.util.UUID;

@RestController
public class FileController {
    private final FileService fileService;

    private final ContentService contentService;

    public FileController(FileService fileService, ContentService contentService) {
        this.fileService = fileService;
        this.contentService = contentService;
    }

    @RequestMapping(value = "/api/1/file",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            method = RequestMethod.POST)
    public UUID saveFile(@RequestPart("file") MultipartFile part){
        return fileService.saveFile(part);
    }

    @RequestMapping(value = "/api/1/file/{uuidFile}", method = RequestMethod.GET)
    public ResponseEntity<Resource> getFile(@PathVariable String uuidFile) {
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(fileService.getFile(uuidFile));
    }

    @RequestMapping(value = "/api/1/file/{uuidFile}", method = RequestMethod.DELETE)
    public void deleteFile(@PathVariable String uuidFile) {
        fileService.deleteFile(uuidFile);
        contentService.removeFileFromContent(uuidFile);
    }
}
