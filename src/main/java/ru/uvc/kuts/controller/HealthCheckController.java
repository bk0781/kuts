package ru.uvc.kuts.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/1/health")
public class HealthCheckController {
    @GetMapping
    public Map<String, String> healthCheck() {
        return Collections.singletonMap("status", "ALIVE");
    }
}
