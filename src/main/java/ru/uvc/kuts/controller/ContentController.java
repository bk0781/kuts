package ru.uvc.kuts.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.uvc.kuts.config.annotation.UserAuth;
import ru.uvc.kuts.entities.Content;
import ru.uvc.kuts.entities.Node;
import ru.uvc.kuts.service.ContentService;
import ru.uvc.kuts.service.FileService;
import ru.uvc.kuts.service.NodeService;
import ru.uvc.kuts.vo.Role;

@RestController
@RequiredArgsConstructor
public class ContentController {
    private final ContentService contentService;
    private final FileService fileService;

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/content", method = RequestMethod.POST)
    public Content saveContent(@RequestBody Content content) {
        return contentService.save(content);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/api/1/content/{nodeId}", method = RequestMethod.GET)
    public Content getContentByNodeId(@PathVariable long nodeId) {
        return contentService.getContentByNodeId(nodeId);
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/content/{contentId}", method = RequestMethod.DELETE)
    public void removeContent(@PathVariable long contentId) {
        String filename = contentService.remove(contentId);
        fileService.deleteFile(filename);
    }
}
