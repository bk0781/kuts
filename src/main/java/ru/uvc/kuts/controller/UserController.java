package ru.uvc.kuts.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.uvc.kuts.config.annotation.UserAuth;
import ru.uvc.kuts.dto.TokenDTO;
import ru.uvc.kuts.dto.UserAuthDTO;
import ru.uvc.kuts.entities.User;
import ru.uvc.kuts.service.UserService;
import ru.uvc.kuts.vo.Role;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @RequestMapping(path = "/api/1/user/register", method = RequestMethod.POST)
    public TokenDTO registerUser(@RequestBody User user) {
        User registered = userService.createUser(user, Role.USER);
        return userService.generateToken(new UserAuthDTO(registered.getLogin(), registered.getPassword()));
    }

    @RequestMapping(path = "/api/1/user/auth", method = RequestMethod.POST)
    public TokenDTO authUser(@RequestBody UserAuthDTO userAuthDTO) {
        return userService.generateToken(userAuthDTO);
    }

    @UserAuth(Role.USER)
    @RequestMapping(path = "/test", method = RequestMethod.POST)
    public String testFunc() {
        return"HELLO";
    }

    @UserAuth(Role.ADMIN)
    @RequestMapping(path = "/api/1/user/group/{groupId}", method = RequestMethod.GET)
    public List<User> getUsersByGroupId(@PathVariable long groupId) {
        return userService.findUsersByGroupId(groupId);
    }

    @RequestMapping(path = "/adm", method = RequestMethod.GET)
    public TokenDTO registerAdmin() {
        User user = new User();
        user.setLogin("admin");
        user.setPassword("admin");
        user.setName("admin");
        User registred =  userService.createUser(user, Role.ADMIN);
        UserAuthDTO userAuthDTO = new UserAuthDTO(registred.getLogin(), registred.getPassword());
        return userService.generateToken(userAuthDTO);
    }

    @RequestMapping(path = "/user", method = RequestMethod.GET)
    public String changePassword(@RequestParam String login, @RequestParam String password) {
        User user = userService.findFirstByLogin(login);
        user.setPassword(password);
        userService.save(user);
        return "OK";
    }
}
