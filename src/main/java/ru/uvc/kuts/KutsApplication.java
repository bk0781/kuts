package ru.uvc.kuts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class KutsApplication {

    public static void main(String[] args) {
        SpringApplication.run(KutsApplication.class, args);
    }

}
