package ru.uvc.kuts.vo;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.uvc.kuts.dto.UserDTO;
import ru.uvc.kuts.entities.User;

import java.time.LocalDateTime;

@Getter
@Builder
@RequiredArgsConstructor
public class TokenAuth {
    private final UserDTO userDTO;
    private final LocalDateTime expiredDate;
    private final String token;
}
