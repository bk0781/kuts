package ru.uvc.kuts.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Node {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String fullName;

    private Long parentNode;

    private Boolean hasContent;

    private Boolean hasTest;

    private Long orderNode;

    @Fetch(FetchMode.JOIN)
    @OneToOne(mappedBy = "node", cascade = CascadeType.REMOVE)
    @JsonManagedReference
    private Content content;

    @Fetch(FetchMode.JOIN)
    @JsonIgnore
    @OneToOne(mappedBy = "node", cascade = CascadeType.REMOVE)
    private Test test;

    @Transient
    private List<Node> children;
}
