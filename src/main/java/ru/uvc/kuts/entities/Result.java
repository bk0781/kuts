package ru.uvc.kuts.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter @Setter
@Entity
@Table(name = "results")
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Boolean correct;

    @ManyToOne
    @JoinColumn(name = "testResult_id")
    @JsonIgnore
    private TestResult testResult;

    public Result(Boolean correct, TestResult testResult) {
        this.correct = correct;
        this.testResult = testResult;
    }
}
