package ru.uvc.kuts.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserAuthDTO {
    private final String login;
    private final String password;
}
