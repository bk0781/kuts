package ru.uvc.kuts.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class ResultTestDTO {
    private final long testId;
    private final Map<Long, Boolean> resultTest;
}
