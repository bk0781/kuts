package ru.uvc.kuts.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.uvc.kuts.vo.Role;

@Getter
@AllArgsConstructor
public class UserDTO {
    private final long userId;
    private final Role role;
}
