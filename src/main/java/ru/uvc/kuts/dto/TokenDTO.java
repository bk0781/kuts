package ru.uvc.kuts.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import ru.uvc.kuts.vo.Role;

@Getter
@AllArgsConstructor
public class TokenDTO {
    private final String token;
    private final Role role;
}
