package ru.uvc.kuts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.uvc.kuts.entities.Group;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    Group findById(long id);
    Group deleteById(long id);
}
