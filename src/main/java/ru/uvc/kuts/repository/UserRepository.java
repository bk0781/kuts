package ru.uvc.kuts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.uvc.kuts.dto.UserDTO;
import ru.uvc.kuts.entities.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    @Query("select new ru.uvc.kuts.dto.UserDTO(id, role) from User where login = :login and password = :password")
    UserDTO findFirstByLoginAndPassword(String login, String password);
    User findFirstByLogin(String login);
    List<User> findAllByGroupId(long groupId);
}
