package ru.uvc.kuts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.uvc.kuts.entities.Node;

import java.util.List;
import java.util.Optional;

@Repository
public interface NodeRepository extends JpaRepository<Node, Long> {

    @Query("from Node n " +
            "left join fetch n.content " +
            "left join fetch n.test ")
    List<Node> findAllNodes();

    @Query("from Node n " +
            "left join fetch n.content " +
            "left join fetch n.test " +
            "where n.id = :id ")
    Optional<Node> findByIdFetch(long id);

    @Query("from Node n " +
            "left join fetch n.content " +
            "left join fetch n.test " +
            "where n.parentNode = :parentId ")
    Node getFirstByParentNode(long parentId);

    @Query("select count(id) from Node where parentNode = :parentId")
    Long getLastOrder(long parentId);


    @Query("select count(id) from Node where parentNode is null ")
    Long getRootLastOrder();

    @Modifying
    @Query("update Node set orderNode = :newOrder where orderNode = :lastOrder and parentNode = :parentId")
    void updateOrder(long lastOrder, long newOrder, long parentId);

    @Modifying
    @Query("update Node set orderNode = :newOrder where orderNode = :lastOrder and parentNode is null")
    void updateOrderRoot(long lastOrder, long newOrder);

    @Modifying
    @Query("update Node set orderNode = (orderNode - 1) where orderNode > :order and parentNode = :parentId")
    void updateOrderIfRemove(long order, long parentId);

    @Modifying
    @Query("update Node set orderNode = (orderNode - 1) where orderNode > :order and parentNode is null")
    void updateOrderRootIfRemove(long order);
}
