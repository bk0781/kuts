package ru.uvc.kuts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.uvc.kuts.entities.Content;
import ru.uvc.kuts.entities.Node;

import java.util.List;

@Repository
public interface ContentRepository extends JpaRepository<Content, Long> {
    List<Content> findAllByContent(String fileId);
    Content findByNode(Node node);
    Content findByNodeId(long nodeId);
    Content findById(long id);
}
