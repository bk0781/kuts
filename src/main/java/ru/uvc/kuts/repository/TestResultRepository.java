package ru.uvc.kuts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.uvc.kuts.entities.TestResult;

import java.util.List;

@Repository
public interface TestResultRepository extends JpaRepository<TestResult, Long> {
    List<TestResult> findAllByUserIdOrderByCreateDateDesc(long userId);
}
