package ru.uvc.kuts.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.uvc.kuts.entities.Node;
import ru.uvc.kuts.entities.Test;

@Repository
public interface TestRepository extends JpaRepository<Test, Long> {
    Test findById(long id);
    Test findByNode(Node node);


    @Query("from Test t " +
            "join fetch t.node n " +
            "left join fetch n.content c " +
            "join fetch t.questions q ")
    Test findByNodeId(long nodeId);
}
