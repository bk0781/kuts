package ru.uvc.kuts.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

//@ControllerAdvice
public class HandlerException {
    @ExceptionHandler({ForbiddenException.class})
    public ResponseEntity handleForbidden(ForbiddenException ex) {
        return new ResponseEntity(HttpStatus.FORBIDDEN);
    }
}
