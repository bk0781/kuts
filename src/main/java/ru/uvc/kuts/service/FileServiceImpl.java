package ru.uvc.kuts.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

@Service
public class FileServiceImpl implements FileService {
    private final String filesDir;

    public FileServiceImpl(@Value("${kuts.file.path}") String filesDir) {
        this.filesDir = filesDir;
    }

    @Override
    public UUID saveFile(MultipartFile filePart) {
        File directory = new File(filesDir);
        if (!directory.exists() && !directory.mkdir()){
            throw new IllegalStateException("Ошибка создания директории для файлов");
        }

        UUID uuidFile = UUID.randomUUID();
        File uploadedFile = new File(new File(filesDir + "/" + uuidFile).getAbsolutePath());
        try {
            filePart.transferTo(uploadedFile);
        } catch (IOException e) {
            throw new IllegalStateException("save unsuccessful. Message = " + e.getMessage());
        }
        return uuidFile;
    }

    @Override
    public Resource getFile(String uuidFile) {
        return new FileSystemResource(filesDir + "/" + uuidFile);
    }

    @Override
    public void deleteFile(String uuidFile) {
        new File(filesDir + "/" + uuidFile).delete();
    }
}
