package ru.uvc.kuts.service;

import ru.uvc.kuts.entities.TestResult;

import java.util.List;

public interface TestResultService {
    TestResult save(TestResult testResult);
    List<TestResult> findAllTestResultsByUserId(long userId);
}
