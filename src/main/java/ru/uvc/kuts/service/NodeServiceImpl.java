package ru.uvc.kuts.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.uvc.kuts.entities.Node;
import ru.uvc.kuts.repository.NodeRepository;

import java.util.*;

@Service
@RequiredArgsConstructor
public class NodeServiceImpl implements NodeService {
    private final NodeRepository nodeRepository;

    @Override
    @Transactional
    public Node save(Node node) {
        if (node.getId() != null) {
            Node oldNode = nodeRepository.findByIdFetch(node.getId()).get();
            if (node.getParentNode() == null) {
                nodeRepository.updateOrderRoot(node.getOrderNode(), oldNode.getOrderNode());
            } else {
                nodeRepository.updateOrder(node.getOrderNode(), oldNode.getOrderNode(), node.getParentNode());
            }
        }

        return nodeRepository.save(node);
    }

    @Override
    public List<Node> getAll() {
        return nodeRepository.findAllNodes();
    }

    @Override
    public Node getById(long id) {
        return nodeRepository.findByIdFetch(id)
                .orElseThrow(() -> new IllegalStateException("Node not found"));
    }

    @Override
    @Transactional
    public String removeNode(long id) {
        Node node = nodeRepository.findByIdFetch(id).get();
        String fileName = node.getContent() == null ? null : node.getContent().getContent();
        if (getFirstByParentId(id) != null) {
            throw new IllegalStateException("Невозможно удалить элемент, есть зависимые элементы");
        }

        if (node.getParentNode() == null) {
            nodeRepository.updateOrderRootIfRemove(node.getOrderNode());
        } else {
            nodeRepository.updateOrderIfRemove(node.getOrderNode(), node.getParentNode());
        }
        nodeRepository.delete(node);
        return fileName;
    }

    @Override
    public Node getFirstByParentId(long parentId) {
        return nodeRepository.getFirstByParentNode(parentId);
    }


    public List<Node> getRootNodes(List<Node> nodes) {
        List<Node> root = new ArrayList<>();
        nodes.stream()
                .filter(el -> el.getParentNode() == null)
                .forEach(el -> {
                    addChildrenToNode(nodes, el);
                    root.add(el);
                });
        root.sort(Comparator.comparing(Node::getOrderNode));
        return root;
    }

    @Override
    public Long nextOrder(Long parentId) {
        return parentId != null ? nodeRepository.getLastOrder(parentId) : nodeRepository.getRootLastOrder();
    }

    private void addChildrenToNode(List<Node> list, Node root) {
        List<Node> children;
        for (Node node: list)   {
            if (Objects.equals(node.getParentNode(), root.getId())) {
                addChildrenToNode(list, node);
                children = root.getChildren() == null
                        ? new ArrayList<>()
                        : root.getChildren();
                children.add(node);
                root.setChildren(children);
                root.getChildren().sort(Comparator.comparing(Node::getOrderNode));
            }
        }
    }
}
