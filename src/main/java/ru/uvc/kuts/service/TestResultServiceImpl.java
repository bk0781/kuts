package ru.uvc.kuts.service;

import org.springframework.stereotype.Service;
import ru.uvc.kuts.entities.TestResult;
import ru.uvc.kuts.repository.TestResultRepository;

import java.util.List;

@Service
public class TestResultServiceImpl implements TestResultService {
    private final TestResultRepository testResultRepository;

    public TestResultServiceImpl(TestResultRepository testResultRepository) {
        this.testResultRepository = testResultRepository;
    }

    @Override
    public TestResult save(TestResult testResult) {
        return testResultRepository.save(testResult);
    }

    @Override
    public List<TestResult> findAllTestResultsByUserId(long userId) {
        return testResultRepository.findAllByUserIdOrderByCreateDateDesc(userId);
    }
}
