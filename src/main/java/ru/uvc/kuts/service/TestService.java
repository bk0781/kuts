package ru.uvc.kuts.service;

import ru.uvc.kuts.dto.ResultTestDTO;
import ru.uvc.kuts.dto.UserDTO;
import ru.uvc.kuts.entities.Node;
import ru.uvc.kuts.entities.Test;
import ru.uvc.kuts.entities.User;

public interface TestService {
    Test save(Test test);
    Test getById(long id);
    void remove(long testId);
    Test getTestByNode(Node node);
    Test getTestByNodeId(long nodeId);
    Test getTestByNodeIdExcludeCorrect(long nodeId);
    ResultTestDTO validateTest(Test test, UserDTO userDTO);
}
