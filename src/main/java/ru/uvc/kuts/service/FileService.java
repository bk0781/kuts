package ru.uvc.kuts.service;

import org.springframework.core.io.Resource;
import org.springframework.http.codec.multipart.FilePart;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

public interface FileService {
    UUID saveFile(MultipartFile filePart);
    Resource getFile(String uuidFile);
    void deleteFile(String uuidFile);
}
