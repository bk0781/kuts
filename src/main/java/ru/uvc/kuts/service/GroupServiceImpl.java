package ru.uvc.kuts.service;

import org.springframework.stereotype.Service;
import ru.uvc.kuts.entities.Group;
import ru.uvc.kuts.repository.GroupRepository;

import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {
    private final GroupRepository groupRepository;

    public GroupServiceImpl(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public Group createGroup(Group group) {
        return groupRepository.save(group);
    }

    public List<Group> getAllGroups() {
        return groupRepository.findAll();
    }

    public Group findById(long id) {
        return groupRepository.findById(id);
    }

    @Override
    public void deleteById(long id) {
        groupRepository.deleteById(id);
    }
}
