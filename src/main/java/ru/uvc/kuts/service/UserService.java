package ru.uvc.kuts.service;

import ru.uvc.kuts.dto.TokenDTO;
import ru.uvc.kuts.dto.UserAuthDTO;
import ru.uvc.kuts.entities.User;
import ru.uvc.kuts.vo.Role;

import java.util.List;

public interface UserService {
    User createUser(User user, Role role);
    TokenDTO generateToken(UserAuthDTO userAuthDTO);
    List<User> findUsersByGroupId(long groupId);
    User findFirstByLogin(String login);
    void save(User user);
}
