package ru.uvc.kuts.service;

import ru.uvc.kuts.entities.Group;

import java.util.List;

public interface GroupService {

    Group createGroup(Group group);

    List<Group> getAllGroups();

    Group findById(long id);

    void deleteById(long id);

}
