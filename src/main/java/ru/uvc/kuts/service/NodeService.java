package ru.uvc.kuts.service;

import ru.uvc.kuts.entities.Node;

import java.util.List;

public interface NodeService {
    Node save(Node node);

    List<Node> getAll();

    Node getById(long id);

    String removeNode(long id);

    Node getFirstByParentId(long parentId);

    List<Node> getRootNodes(List<Node> nodes);

    Long nextOrder(Long parentId);
}
