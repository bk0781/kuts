package ru.uvc.kuts.service;

import lombok.RequiredArgsConstructor;
import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import ru.uvc.kuts.dto.ResultTestDTO;
import ru.uvc.kuts.dto.UserDTO;
import ru.uvc.kuts.entities.*;
import ru.uvc.kuts.repository.TestRepository;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TestServiceImpl implements TestService {
    private final TestRepository testRepository;
    private final TestResultService testResultService;
    private final NodeService nodeService;

    @Override
    @Transactional
    public Test save(Test test) {
        if (test == null
                || test.getTestName() == null
                || test.getTestName().isEmpty()
                || test.getNode() == null
                || test.getNode().getId() == null
                || CollectionUtils.isEmpty(test.getQuestions())) {
            throw new IllegalStateException("Неправильно заполнен тест");
        }

        Node node = nodeService.getById(test.getNode().getId());
        node.setHasTest(true);
        nodeService.save(node);

        test.getQuestions().forEach(q -> {
            q.setTest(test);
            q.getChoices().forEach(c -> c.setQuestion(q));
        });

        return testRepository.save(test);
    }

    @Override
    public Test getById(long id) {
        return testRepository.findById(id);
    }

    @Override
    @Transactional
    public void remove(long testId) {
        Test test = getById(testId);
        Node node = nodeService.getById(test.getNode().getId());

        node.setHasTest(false);
        nodeService.save(node);

        testRepository.delete(test);
    }

    @Override
    public Test getTestByNode(Node node) {
        return testRepository.findByNode(node);
    }

    @Override
    public Test getTestByNodeId(long nodeId) {
        return testRepository.findByNodeId(nodeId);
    }

    @Override
    public Test getTestByNodeIdExcludeCorrect(long nodeId) {
        Test test = testRepository.findByNodeId(nodeId);
        if (test == null || CollectionUtils.isEmpty(test.getQuestions())) {
            throw new IllegalStateException("Test not found");
        }
        test.getQuestions().stream()
                .filter(e -> !CollectionUtils.isEmpty(e.getChoices()))
                .flatMap(e -> e.getChoices().stream())
                .forEach(e -> e.setCorrect(false));
        return test;
    }

    @Override
    @Transactional
    public ResultTestDTO validateTest(Test test, UserDTO userDTO) {
        Test testRep = testRepository.findById(test.getId().longValue());
        ResultTestDTO resultTestDTO = checkTestAnswers(test, testRep);
        TestResult testResult = new TestResult();
        testResult.setCreateDate(LocalDateTime.now().atZone(ZoneId.of("Europe/Moscow")).toInstant().toEpochMilli());

        List<Result> resultsList = new ArrayList<Result>();
        resultTestDTO.getResultTest()
                .forEach((id, cor) -> {
                    resultsList.add(new Result(cor, testResult));
                });

        testResult.setResultTest(resultsList);
        testResult.setTest(test);

        User user = new User();
        user.setId(userDTO.getUserId());
        testResult.setUser(user);

        testResultService.save(testResult);
        return resultTestDTO;
    }

    private ResultTestDTO checkTestAnswers(Test answer, Test testRep) {
        ResultTestDTO result = new ResultTestDTO(answer.getId(), new HashMap<>());

        List<Question> questionRep = testRep.getQuestions();
        List<Question> questionAnswer = answer.getQuestions();
        if ((questionRep.size() == 0) && (questionAnswer.size() == 0)) {
            return result; // Сразу правильно за весь тест
        }

        questionRep.forEach(qRep -> {
            Question ans = questionAnswer.stream().filter(qAns -> qAns.getId().equals(qRep.getId())).findFirst().orElse(null);
            List<Choice> choiceRep = qRep.getChoices() == null
                    ? Collections.emptyList()
                    : qRep.getChoices().stream().filter(e -> e.getCorrect() != null && e.getCorrect()).collect(Collectors.toList());

            List<Choice> choiceAns = ((ans == null) || (ans.getChoices() == null))
                    ? Collections.emptyList()
                    : ans.getChoices().stream().filter(e -> e.getCorrect() != null && e.getCorrect()).collect(Collectors.toList());

            if (choiceRep.size() != choiceAns.size()) {
                result.getResultTest().put(qRep.getId(), false);
                return;
            }

            List<Choice> isPresentInAnswer = choiceRep.stream()
                    .filter(chRep -> choiceAns.stream().anyMatch(chAns -> chAns.getId().equals(chRep.getId())))
                    .collect(Collectors.toList());

            if (choiceRep.size() != isPresentInAnswer.size()) {
                result.getResultTest().put(qRep.getId(), false);
            } else {
                result.getResultTest().put(qRep.getId(), true);
            }
        });

        return result;
    }
}
