package ru.uvc.kuts.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.uvc.kuts.entities.Content;
import ru.uvc.kuts.entities.Node;
import ru.uvc.kuts.repository.ContentRepository;
import ru.uvc.kuts.repository.NodeRepository;

@Service
@RequiredArgsConstructor
public class ContentServiceImpl implements ContentService {
    private final ContentRepository contentRepository;
    private final NodeService nodeService;

    @Override
    @Transactional
    public Content save(Content content) {
        Node node = nodeService.getById(content.getNode().getId());
        node.setHasContent(true);
        nodeService.save(node);

        return contentRepository.save(content);
    }

    @Override
    public Content getById(long id) {
        return contentRepository.findById(id);
    }

    @Override
    @Transactional
    public String remove(long contentId) {
        Content content = getById(contentId);
        Node node = nodeService.getById(content.getNode().getId());

        String filename = content.getContent();
        node.setHasContent(false);
        nodeService.save(node);
        contentRepository.delete(content);
        return filename;
    }

    @Override
    public void removeFileFromContent(String fileId) {
         contentRepository
                 .findAllByContent(fileId)
                 .forEach(content -> {
                     content.setContent(null);
                     save(content);
                 });
    }

    @Override
    public Content getContentByNode(Node node) {
        return contentRepository.findByNode(node);
    }

    @Override
    public Content getContentByNodeId(long nodeId) {
        return contentRepository.findByNodeId(nodeId);
    }
}
