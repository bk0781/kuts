package ru.uvc.kuts.service;

import ru.uvc.kuts.entities.Content;
import ru.uvc.kuts.entities.Node;

public interface ContentService {
    Content save(Content content);
    Content getById(long id);
    String remove(long contentId);
    void removeFileFromContent(String fileId);
    Content getContentByNode(Node node);
    Content getContentByNodeId(long nodeId);
}
