package ru.uvc.kuts.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.uvc.kuts.config.annotation.AuthTokenMap;
import ru.uvc.kuts.dto.TokenDTO;
import ru.uvc.kuts.dto.UserAuthDTO;
import ru.uvc.kuts.dto.UserDTO;
import ru.uvc.kuts.entities.User;
import ru.uvc.kuts.repository.UserRepository;
import ru.uvc.kuts.vo.Role;
import ru.uvc.kuts.vo.TokenAuth;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final Map<String, TokenAuth> authTokenMap;

    // in minutes
    private final int tokenLifeTime;

    public UserServiceImpl(UserRepository userRepository,
                           @AuthTokenMap Map<String, TokenAuth> authTokenMap,
                           @Value("${auth.token.lifetime}") int tokenLifeTime) {
        this.userRepository = userRepository;
        this.authTokenMap = authTokenMap;
        this.tokenLifeTime = tokenLifeTime;
    }

    @Override
    public User createUser(User user, Role role) {
        User existUser = findFirstByLogin(user.getLogin());
        if (existUser != null) {
            throw new IllegalStateException("Пользователь с указанным логином уже существует");
        }
        user.setRole(role);
        return userRepository.save(user);
    }

    @Override
    public TokenDTO generateToken(UserAuthDTO userAuthDTO) {
        UserDTO user = userRepository.findFirstByLoginAndPassword(userAuthDTO.getLogin(), userAuthDTO.getPassword());
        if (user == null) {
            throw new IllegalStateException("Авторизация прошла неудачно");
        }
        String authToken = UUID.randomUUID().toString();
        authTokenMap.put(
                authToken,
                TokenAuth.builder()
                        .token(authToken)
                        .expiredDate(LocalDateTime.now().plusMinutes(tokenLifeTime))
                        .userDTO(user)
                        .build()
        );
        return new TokenDTO(authToken, user.getRole());
    }

    @Override
    public List<User> findUsersByGroupId(long groupId) {
        return userRepository.findAllByGroupId(groupId);
    }

    @Override
    public User findFirstByLogin(String login) {
        return userRepository.findFirstByLogin(login);
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }
}
