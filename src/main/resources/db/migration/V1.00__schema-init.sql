create sequence HIBERNATE_SEQUENCE;

create table GROUPS
(
    ID   BIGINT not null
        primary key,
    NAME VARCHAR(255)
);

create table NODE
(
    ID          BIGINT not null
        primary key,
    FULL_NAME   VARCHAR(255),
    HAS_CONTENT BOOLEAN,
    HAS_TEST    BOOLEAN,
    PARENT_NODE BIGINT,
    ORDER_NODE BIGINT
);

create table CONTENT
(
    ID           BIGINT not null
        primary key,
    CONTENT      VARCHAR(255),
    CONTENT_TYPE INTEGER,
    EXECUTE_FILE VARCHAR(255),
    HEADER       VARCHAR(255),
    NODE_ID      BIGINT,
    constraint FKEBENP8166WJNCK5OPF7E9I33B
        foreign key (NODE_ID) references NODE (ID)
);

create table TEST
(
    ID        BIGINT not null
        primary key,
    TEST_NAME VARCHAR(255),
    NODE_ID   BIGINT,
    constraint FKM52UAAY6O2476C2SOSQRJK302
        foreign key (NODE_ID) references NODE (ID)
);

create table QUESTION
(
    ID            BIGINT not null
        primary key,
    QUESTION_NAME TEXT,
    TEST_ID       BIGINT,
    constraint FK8HEJCPBBIQ1QJE11346AKP3UJ
        foreign key (TEST_ID) references TEST (ID)
);

create table CHOICE
(
    ID          BIGINT not null
        primary key,
    CHOICE_NAME TEXT,
    CORRECT     BOOLEAN default FALSE,
    QUESTION_ID BIGINT,
    constraint FKCAQ6R76CSWKE5B9FK6FYX3Y5W
        foreign key (QUESTION_ID) references QUESTION (ID)
);

create table USERS
(
    ID       BIGINT not null
        primary key,
    LOGIN    VARCHAR(255),
    NAME     VARCHAR(255),
    PASSWORD VARCHAR(255),
    ROLE     VARCHAR(255),
    GROUP_ID BIGINT,
    constraint FKEMFUGLPRP85BH5XWHFM898YSC
        foreign key (GROUP_ID) references GROUPS (ID)
);

create table TEST_RESULT
(
    ID          BIGINT not null
        primary key,
    CREATE_DATE BIGINT,
    TEST_ID     BIGINT,
    USER_ID     BIGINT,
    constraint FK7Q1XFJWUJ28SWDNA3B0B5LCOL
        foreign key (USER_ID) references USERS (ID),
    constraint FKEF3E8K7FGVKJ4MOX0LXRKF8HH
        foreign key (TEST_ID) references TEST (ID)
);

create table RESULTS
(
    ID             BIGINT not null
        primary key,
    CORRECT        BOOLEAN,
    TEST_RESULT_ID BIGINT,
    constraint FK8P8W29RHV2MJM81DGYOH0NIXD
        foreign key (TEST_RESULT_ID) references TEST_RESULT (ID)
);

insert into users (id, login, name, password, role) values (nextval('HIBERNATE_SEQUENCE'), 'admin', 'admin', 'admin', 'ADMIN');
